#!/usr/bin/env nix-shell
# NixOS shell configuration to bootstrap the required dependencies to run and use the editor

# NOTE(Krey): This is designed to contain ONLY editor-specific configuration **NOT** project specific

{
	sources ? import ../../../25-env/nix/niv/sources.nix,
	nixpkgs ? import sources.nixpkgs {}
}:
with nixpkgs;
	stdenv.mkDerivation {
		name = "RiXotStudio_codium-config";
		buildInputs = [
			fira-code # Default font with liqurature support
			texlive.combined.scheme-full # Used to edit TeX files
			vscodium # Editor
		];

		shellHook = ''
			# Status update
			printf "\\n%s\\n" "Standard RiXotStudio development environment for vscodium has been set up"
		'';
	}
